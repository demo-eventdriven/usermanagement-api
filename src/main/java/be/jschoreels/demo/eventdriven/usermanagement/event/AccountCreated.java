package be.jschoreels.demo.eventdriven.usermanagement.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = AccountCreated.Builder.class)
public class AccountCreated {

    private String username;

    private String phoneNumber;

    private String email;

    private AccountCreated(Builder builder) {
        username = builder.username;
        phoneNumber = builder.phoneNumber;
        email = builder.email;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AccountCreated copy) {
        Builder builder = new Builder();
        builder.username = copy.getUsername();
        builder.phoneNumber = copy.getPhoneNumber();
        builder.email = copy.getEmail();
        return builder;
    }

    public String getUsername() {
        return username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    @JsonPOJOBuilder
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Builder {
        private String username;
        private String phoneNumber;
        private String email;

        private Builder() {
        }

        public Builder withUsername(String val) {
            username = val;
            return this;
        }

        public Builder withPhoneNumber(String val) {
            phoneNumber = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public AccountCreated build() {
            return new AccountCreated(this);
        }
    }
}
