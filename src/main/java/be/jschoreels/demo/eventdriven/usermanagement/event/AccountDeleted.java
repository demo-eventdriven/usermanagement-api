package be.jschoreels.demo.eventdriven.usermanagement.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = AccountDeleted.Builder.class)
public class AccountDeleted {

    private String username;

    private AccountDeleted(Builder builder) {
        username = builder.username;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(AccountDeleted copy) {
        Builder builder = new Builder();
        builder.username = copy.getUsername();
        return builder;
    }

    public String getUsername() {
        return username;
    }

    @JsonPOJOBuilder
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Builder {
        private String username;

        private Builder() {
        }

        public Builder withUsername(String val) {
            username = val;
            return this;
        }

        public AccountDeleted build() {
            return new AccountDeleted(this);
        }
    }
}
